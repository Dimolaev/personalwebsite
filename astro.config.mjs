import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
  site: 'https://Dimolaev.gitlab.io',
  base: '/personalwebsite',
  outDir: 'public',
  publicDir: 'static',
});
